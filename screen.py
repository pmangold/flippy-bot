# very useful information in the doc
# https://python-mss.readthedocs.io/examples.html

import mss
from PIL import Image
import time
from pynput import mouse
import os
if not os.path.exists("Screenshots"):
    os.makedirs("Screenshots")

class Screen():
    def __init__(self):
        self.sct = mss.mss()


    def retrieve_click(self, x, y, button, pressed):
        if self.top != -1 and self.bottom != -1:
            return False

        if pressed:
            if self.top == -1:
                self.top = (x, y)

            else:
                self.bottom = (x, y)


    def set_screen_limits(self, default=None):
        # if no screen limits provided, ask the user to click it
        if default is None:
            print("Click on top left and bottom right corners (in that order).")

            self.top = -1
            self.bottom = -1

            with mouse.Listener(on_click=self.retrieve_click) as listener:
                listener.join()

        # if limits provided, use it
        else:
            self.top = default[0]
            self.bottom = default[1]

        self.width = self.bottom[0] - self.top[0]
        self.height = self.bottom[1] - self.top[1]


        self.monitor = {"top": self.top[1],
                        "left": self.top[0],
                        "width": self.width,
                        "height": self.height}

        print("Borders of screen set to : ", self.top, " and ", self.bottom, ".", sep="")


    def update(self):
        self.last_tick = time.time()

        sct_img = self.sct.grab(self.monitor)

        self.screen = Image.frombytes('RGB', sct_img.size, sct_img.bgra, 'raw', 'BGRX')


    def get_pixels_dict(self, pts):
        ret = {}

        for key in pts:
            ret[key] = self.screen.getpixel(pts[key])

        return ret

    def output_image(self,rank):
        """Saves a screenshot as 'Screenshot/image-$(rank).png'"""
        filename = self.sct.shot(mon=-1, output='Screenshots/image-'+str(rank)+'.png')
        print(filename)
