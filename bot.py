import time
from pynput.keyboard import Key, Controller

from screen import Screen
from game_mechanics import *
from detector import Detector
from verbose import Verbose

### VERBOSE LEVEL
# set verbose to
# 0 - no output
# 1 - show movements
# 2 - show movements + danger (instant and duration)

#verbose = Verbose()
verbose = Verbose(trigger = "never", position = True, danger = True, screen = "never", levelup = True, faster = True)


### SETUP

# initialize screen
screen = Screen()

#Paul's defaults
#default = ((44, 155), (903, 688))
#Use the following line if you play the game for the first time
# if None, it asks to click to set up the screen
default = None
#Hippolyte's defaults :
#default = ((106, 227),(1067, 829))

screen.set_screen_limits(default = default)

# initialize keyboard
keyboard = Controller()

# game handler
game = Game(screen, keyboard, verbose=verbose)

# get points of useful elements
pts = game.get_points()

# set up detector with appropriate thresholds
thresholds = {"flame" : 100, "knife": 240}
detector = Detector(screen, pts, thresholds)


### BOT LOGIC (!!)


# loop foreeeeever
while True:

    # update screen and detect relevant pixels
    screen.update()

    # detect dangers
    danger = detector.detect()

    # update the game information
    game.tick(danger)
    


    ##### THIS IS WHERE THE BOT LOGIC IS !!

    # some notes :
    # game.last_move["type"] is type of last move ("jump" or "roll")
    # game.fish_pos is the fish position (0 = top, 1 = middle, 2 = bottom)
    # danger["flame"][i] indicates the presence of a flame thrower at position i
    # danger["knife"] indicates the presence of the knife
    # danger["burn"][i] indicates that the flame thrower i is activated

    # have fun botting this game

    # time to move !

    if game.duration_danger[3] > game.knife_threshold :
        game.jump()

    elif game.fish_pos == 0 and danger["flame"][0] and not danger["burn"][1]:
        game.move_down()
    elif game.fish_pos == 2 and danger["flame"][2] and not danger["burn"][1]:
        game.move_up()

    elif game.fish_pos == 1 and danger["flame"][1]:
        if not (danger["flame"][0] or danger["burn"][0]):
            game.move_up()
        elif not (danger["flame"][2] or danger["burn"][2]):
            game.move_down()

    # print what has to be printed
    if game.changed == True:
        game.verbate("danger")
    if game.pos_changed == True :
        game.verbate("move")
    game.verbate("loop")
