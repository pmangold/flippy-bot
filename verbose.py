class Verbose:
    def __init__(self, danger=False, time=False, position=False, trigger="never", screen = "never", levelup = False, faster = False):
        self.danger = danger
        self.time = time
        self.position = position
        self.trigger = trigger
        self.screen = screen
        self.levelup = levelup
        self.faster = faster
