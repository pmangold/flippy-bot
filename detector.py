class Detector():
    def __init__(self, screen, pts, thresholds):
        self.screen = screen
        self.pts = pts

        # colors for detecting elements
        self.thresholds = thresholds


    def detect(self):
        self.screen.update()
        pix = self.screen.get_pixels_dict(self.pts)

        return {"flame": self.detect_flames(pix),
                "knife": self.detect_knife(pix),
                "burn": self.detect_burn(pix),
                "position": self.detect_position(pix),
                "levelup" : self.detect_levelup(pix)}

    def detect_flames(self, pix):
        # detect flames
        return (pix["flame0"][2] < self.thresholds["flame"] or pix["flame02"][2] == 0,
                pix["flame1"][2] < self.thresholds["flame"] or pix["flame12"][2] == 0,
                pix["flame2"][2] < self.thresholds["flame"] or pix["flame22"][2] == 0)

    def detect_knife(self, pix):
        # detect knife
        return (pix["boardbl"][0] < self.thresholds["knife"] \
            and pix["boardbr"][0] < self.thresholds["knife"]) \
            or (pix["boardtl"][0] < self.thresholds["knife"] \
            and pix["boardtr"][0] < self.thresholds["knife"])

    def detect_burn(self, pix):
        # detect burning flames
        return (pix["flame02"][2] == 0,
                pix["flame12"][2] == 0,
                pix["flame22"][2] == 0)

    def detect_position(self,pix):
        """Detect the position of the fish by checking whether three points on the eye are grey
        if the fish is moving, returns -1"""                                                                                                                                                             
        if pix["eye0a"][0] == pix["eye0a"][1] and pix["eye0a"][1] == pix["eye0a"][2] and pix["eye0b"][0] == pix["eye0b"][1] and pix["eye0b"][1] == pix["eye0b"][2] and pix["eye0c"][0] == pix["eye0c"][1] and pix["eye0c"][1] == pix["eye0c"][2]:
            return 0

        elif pix["eye1a"][0] == pix["eye1a"][1] and pix["eye1a"][1] == pix["eye1a"][2] and pix["eye1b"][0] == pix["eye1b"][1] and pix["eye1b"][1] == pix["eye1b"][2] and pix["eye1c"][0] == pix["eye1c"][1] and pix["eye1c"][1] == pix["eye1c"][2]:
            return 1

        if pix["eye2a"][0] == pix["eye2a"][1] and pix["eye2a"][1] == pix["eye2a"][2] and pix["eye2b"][0] == pix["eye2b"][1] and pix["eye2b"][1] == pix["eye2b"][2] and pix["eye2c"][0] == pix["eye2c"][1] and pix["eye2c"][1] == pix["eye2c"][2]:
            return 2

        else:
            return -1

    def detect_levelup(self,pix):
        """Detects a level up and if it is 150 or 200"""
        return (pix["levelup"] == (255,255,255) and pix["levelup2"] == (255,255,255),
                pix["150a"] == (85,84,84) and pix["150b"] == (85,84,84),
                pix["200"] == (85,84,84) )


