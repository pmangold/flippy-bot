import time
from pynput.keyboard import Key

FLAME0 = 0
FLAME1 = 1
FLAME2 = 2
KNIFE = 3
BURN0 = 4
BURN1 = 5
BURN2 = 6


class Game:
    def __init__(self, screen, keyboard, verbose=0):
        # screen and keyboards
        self.screen = screen
        self.keyboard = keyboard

        # initialization
        self.verbose = verbose
        self.fish_pos = 1

        self.last_move = {"type": "jump", "time": time.time()}

        self.time_no_danger = [time.time()] * 4
        self.duration_danger = [0., 0., 0., 0.]

        self.rank=0
        self.changed=False
        self.pos_changed = False
        
        self.instant_danger = {"flame": (False, False, False),
                               "knife": False,
                               "burn": (False, False, False),
                               "position" : 1}

        # Time before jumping when a knife appears
        # Decreases at 150 and 200
        self.knife_threshold = 0.43 

    def tick(self, instant_danger):
        self.update_instant_danger(instant_danger)
        self.update_danger_times()
        self.check_speedup()
            
    def update_instant_danger(self, instant_danger):
        """If instant_danger differs from self,
        update it and write it updated"""
        if instant_danger["position"] != self.fish_pos:
            self.fish_pos = instant_danger["position"]
            self.pos_changed = True
        else :
            self.pos_changed = False
        if not self.instant_danger == instant_danger:
            self.instant_danger = instant_danger
            self.changed=True
        else:
            self.changed=False

    def update_danger_times(self):

        current_time = time.time()

        # update danger time of flames
        for i in range(3):
            if not self.instant_danger["flame"][i]:
                self.time_no_danger[i] = current_time

        # update danger time of knife
        if not self.instant_danger["knife"]:
            self.time_no_danger[3] = current_time

        self.duration_danger = [current_time - t for t in self.time_no_danger]


    def base_move(self, move_type):
        # note that this function is executed by each move
        if self.verbose.trigger == "action" :
            print(move_type)

        self.last_move = {"type": move_type, "time": time.time()}

    # move up
    def move_up(self):
        self.base_move("roll up")

        self.keyboard.press(Key.up)
        self.keyboard.release(Key.up)


    # move down
    def move_down(self):
        self.base_move("roll down")

        self.keyboard.press(Key.down)
        self.keyboard.release(Key.down)


    # jump
    def jump(self):
        self.base_move("jump")

        self.keyboard.press(Key.space)
        self.keyboard.release(Key.space)


    # get points on screen
    def get_points(self):
        pts = {}

        width, height = self.screen.width, self.screen.height

        pts["flame0"] = (int(0.6663318 * width),
                         int(0.376254 * height))
        pts["flame1"] = (int(0.6652719 * width),
                         int(0.533444816 * height))
        pts["flame2"] = (int(0.657949 * width),
                         int(0.739130434 * height))


        pts["flame02"] = (int(0.041884816753926704 * width),
                          int(0.2902684563758389 * height))
        pts["flame12"] = (int(0.041884816753926704 * width),
                          int(0.4563758389261745 * height))
        pts["flame22"] = (int(0.041884816753926704 * width),
                          int(0.6325503355704698 * height))

        pts["knife"] =  (int(0.084728033 * width),
                         int(0.295986622 * height))

        pts["eye0a"]= (int(width * (516-99)/891),int(height * (434-286)/555))
        pts["eye0b"]= (int(width * (551-99)/891),int(height * (384-286)/555))
        pts["eye0c"]= (int(width * (592-99)/891),int(height * (427-286)/555))

        pts["eye1a"]= (int(width * (515-99)/891),int(height * (524-286)/555))
        pts["eye1b"]= (int(width * (550-99)/891),int(height * (481-286)/555))
        pts["eye1c"]= (int(width * (592-99)/891),int(height * (520-286)/555))

        pts["eye2a"]= (int(width * (519-99)/891),int(height * (628-286)/555))
        pts["eye2b"]= (int(width * (551-99)/891),int(height * (572-286)/555))
        pts["eye2c"]= (int(width * (594-99)/891),int(height * (617-286)/555))

        pts["levelup"]= (int(width * (363-106)/959),int(height * (487-227)/600))
        pts["levelup2"]= (int(width * (353-106)/959),int(height * (487-227)/600))
        pts["150a"]= (int(width * (1012-106)/959),int(height * (337-227)/600))
        pts["150b"]= (int(width * (1038-106)/959),int(height * (349-227)/600))
        pts["200"]= (int(width * (1010-106)/959),int(height * (356-227)/600))

        pts["boardtl"] = (int(width * (180-99)/891), int (height * (447-286)/555))
        pts["boardtr"] = (int(width * (686-99)/891), int (height * (449-286)/555))
        pts["boardbr"] = (int(width * (720-99)/891), int (height * (719-286)/555))
        pts["boardbl"] = (int(width * (114-99)/891), int (height * (714-286)/555))
        
        return pts

    def verbate(self, trigger) :
        """Print or take a screenshot if verbose is set to doing so on trigger"""
        if self.verbose.trigger == trigger :
            if self.verbose.danger :
                print(self.instant_danger)
            if self.verbose.time :
                print(self.duration_danger)
            if self.verbose.position :
                print(self.fish_pos)
        if self.verbose.screen == trigger :
            self.rank +=1
            self.screen.output_image(self.rank)

    def check_speedup(self) :
        """Updates knife_threshold when levelling up at 150 and 200"""
        if self.instant_danger["levelup"][0] :
            if self.verbose.levelup :
                print("Level up!")
            if self.instant_danger["levelup"][2] :
                self.knife_threshold = 0.23
                if self.verbose.faster :
                    print("Faster!!")
            elif self.instant_danger["levelup"][1] :
                self.knife_threshold = 0.3
                if self.verbose.faster :
                    print("Faster now")
