# FLIPPY BOT

This is a bot for the game Flippy Fish, from the LD game Jam. You can play it on this page : https://ldjam.com/events/ludum-dare/46/flippy-fish.

I want to thank the creator of this game, it is quite fun, but it is even funnier when watching a bot play.

Also thanks Instafluff who had the idea of designing a bot for this game. It was done on a live stream here : https://www.twitch.tv/videos/601271534.

You can run the bot on any version of the game on any platform (I think the python library are cross platforms...), by running the script bot.py :

	python bot.py
	
It will ask you to click on the upper left and bottom right corner of the game window. You can then click play and watch it play.

I have seen it score 92, I know people do better. There are still some bugs and the code is messy, so there is still plenty of room for improvement so... beware!



# Known Issues...

Does not detect knife when there are flames everywhere

# Best scores

Best known score on Flippy Fish : 237 by Fishmasterino

Best known score using Flippy Bot : 223 